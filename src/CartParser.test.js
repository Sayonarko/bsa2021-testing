import CartParser from './CartParser';

let parser, parse, validate;

beforeEach(() => {
	parser = new CartParser();
	parse = parser.parse.bind(parser);
	validate = parser.validate.bind(parser);
});

describe('CartParser - unit tests', () => {
	// Add your unit tests here.

	//validate
	it('validate should return empty array with valid contents', () => {
		const contents = `Product name,Price,Quantity
											Mollis consequat,9.00,2
											Tvoluptatem,10.32,1
											Scelerisque lacinia,18.90,1
											Consectetur adipiscing,28.72,10
											Condimentum aliquet,13.90,1`;

		expect(validate(contents)).toEqual([]);
	});

	it('validate should return array with error type "header"', () => {
		const contents = 'Product name,"",Quantity';
		const message = 'Expected header to be named \"Price\" but received \"\".';

		expect(...validate(contents)).toHaveProperty('type', 'header');
		expect(...validate(contents)).toHaveProperty('message', message);
	});

	it('validate should return array with error type "row"', () => {
		const contents = `Product name,Price,Quantity
											 Mollis consequat,2`;
		const message = 'Expected row to have 3 cells but received 2.';

		expect(...validate(contents)).toHaveProperty('type', 'row');
		expect(...validate(contents)).toHaveProperty('message', message);
	});

	it('validate should return array with error type "cell"', () => {
		const contents = `Product name,Price,Quantity
											Mollis consequat,9.00,-2`;
		const message = 'Expected cell to be a positive number but received \"-2\".';

		expect(...validate(contents)).toHaveProperty('type', 'cell');
		expect(...validate(contents)).toHaveProperty('message', message);
	});

	it('validate should return array with error type "cell"', () => {
		const contents = `Product name,Price,Quantity
											 ,9.00,2`;
		const message = 'Expected cell to be a nonempty string but received \"\".';

		expect(...validate(contents)).toHaveProperty('type', 'cell');
		expect(...validate(contents)).toHaveProperty('message', message);
	});
});

describe('CartParser - integration test', () => {
	// Add your integration test here.

	it('parse should return a JSON object whith property: "items", "total"', () => {
		const cart = parse('./samples/cart.csv');

		expect(cart).toHaveProperty('items');
		expect(cart).toHaveProperty('total');
		expect(Array.isArray(cart['items'])).toBe(true);
		expect(Number(cart['total'])).toBe(cart['total']);
	});

	it('parse should trow error if contents has validationErrors', () => {
		expect(() => parse('./samples/incorrectCart.csv')).toThrow("Validation failed!");
	});
});